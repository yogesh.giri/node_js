const mongoose = require('mongoose');
const ProductSchema = new mongoose.Schema({
    name: {type: String, required: true},
    age: {type: Number, required: true},
});


// Export the model
const Product = new mongoose.model('Product', ProductSchema);
module.exports = Product;