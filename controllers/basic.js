const express = require('express');
const path = require('path');

const app = express();
const folderPath = path.join(__dirname, '/views');

//for parsing incomming data
app.use(express.json())

//form-data parse
app.use(express.urlencoded({extended: false}))

app.get('/home',(req, res)=>{
  res.sendFile(`${folderPath}/home.html`);
});

app.get('/', (req, res)=>{
  res.send('index')
});

app.get('/data',(req, res)=>{
  let q = req.query.age
  res.json({"home":q})
});

app.post('/post', (req, res) => {
   console.log('boy', req.body)
   const {name, age} = req.body
  //  const name1 = req.body.name
   res.json({"name1":name, "age": age})
})

app.listen(5000, ()=>{
  console.log(folderPath)
});