const express = require('express');
const app = express();
const port = process.env.PORT || 5000;
const axios = require('axios')

app.use(express.json())

require('./config/db')
const Product = require('./models/model')

app.get('/hello', async (req, res)=>{
   try {
    const {id} = req.query
    let response = await axios.get(`https://jsonplaceholder.typicode.com/posts?id=${id}`)
    data = response.data
    res.json({data})
   } catch (error) {
     console.log(error)
   }
})

app.post('/post', async(req, res)=>{
    try{
        // const data = {
        //     "name":"yogesh",
        //     "age":"20"
        // }
        const insert = new Product(req.body);
        const result = await insert.save();
        res.send(result);
    }catch(e){
        res.status(400).send(e);
    }
})

app.get('/get', async(req, res)=>{
    try{
        // const select = await Product.findOne();
        // const select = await Product.find();
        const insert = req.body;
        res.send(insert);
        // const select = await Product.findAndDelete({"name":"yogesh "});

        // res.send(select);
    }catch(e){
        res.status(400).send(e);
    }
})

app.listen(port,()=>{
    console.log('Connected')
})