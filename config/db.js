const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/Student',  {
  useNewUrlParser:true
}).then(()=>{
  console.log("DB connected")
}).catch((e)=>{
  console.log(`DB not connected ${e}`)
})
